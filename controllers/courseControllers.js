const Course = require('../models/Course');

const auth = require('../auth')

const bcrypt = require('bcrypt');
const req = require('express/lib/request');

module.exports.getAllCourses = () => {

    return Course.find().then( (result, error) => {
        if(result !== null){
            return result
        } else {
            return error
        }
    })
}

module.exports.createCourse = (reqBody) => {
    
    let newCourse = new Course ({
        courseName:reqBody.courseName,
        description:reqBody.description,
        price:reqBody.price,
        isActive: reqBody.isActive
        
    })

    return newCourse.save().then((result,error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

