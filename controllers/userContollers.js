// Business Logic - includes the model methods

//user model

const User = require('../models/User');

const auth = require('../auth')

const bcrypt = require('bcrypt');
const Course = require('../models/Course');

module.exports.checkEmail = (email) => {
    return User.findOne({email: email}).then((result,error) => {
        console.log(result)
        
        if(result !== null){
            return(false)
        }else{
            
            if(result === null){
                //send back the response to the client
                return(true)
            }else{
                return(error)
            }
        }
    })
    
}

module.exports.register = (reqBody) => {
    //save or create a new user document
    //using .save()method to save 
    //console.log(reqBody)

    const {firstName, lastName, email, password, mobileNo, age} = reqBody

    const newUser = new User ({
        firstName:firstName,
        lastName:lastName,
        email:email,
        password:bcrypt.hashSync(password, 10),
        mobileNo:mobileNo,
        age:age

    })
    return newUser.save().then((result,error) => {
        if(result){
            return true
        }else{
            return error
        }
    })
}


module.exports.getAllUsers = () => {

    //difference between findOne() & find()
		//findOne() returns one document
		//find() returns an array of documents []


    return User.find().then( (result, error) => {
        if(result !== null){
            return result
        } else {
            return error
        }
    })
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody

	return User.findOne({email: email}).then( (result, error) => {
		// console.log(result)

		if(result == null){
			console.log('email null');
			return false
		} else {

			let isPwCorrect = bcrypt.compareSync(password, result.password)	//boolean bec it compares two arguments

			if(isPwCorrect == true){
			//return json web token
				//invoke the function which creates the token upon logging in
				// requirements in creating a token:
					// if password matches from existing pw from db
					return {access: auth.createAccessToken(result)}
					// return auth.createAccessToken(result)
			} else {
				return false
			}
		}
	})
}


module.exports.getUserProfile = (reqBody) => {
	
	/*mini activity*/	
	// using findById method, look for the matching document and return the matching document to the client

	return User.findById({_id: reqBody.id}).then(result => {
			return result;
		})
}


module.exports.editProfile = (userId, reqBody) => {
	// console.log(userId)
	// console.log(reqBody)

	return User.findByIdAndUpdate(userId, reqBody, {new:true}).then( result => {
		// console.log(result)

		result.password="****"

		return result
	})

}


module.exports.editUser = (userId, reqBody) => {
	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}

	return User.findByIdAndUpdate(userId, updatedUser, {new:true}).then( result => {
		
		result.password = "****"
		return result
	})
}


module.exports.editDetails = (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}


	return User.findOneAndUpdate({email: email}, updatedUser, {returnDocument:'after'}).then( result => {
		result.password = "****"

		return result
	})
}


module.exports.delete = (userId) => {
	// console.log(userId)

	return User.findByIdAndDelete(userId).then( result => {
		// console.log(result)

		if(result){
			return true
		}
	})
}

module.exports.deleteUser = (email) => {
	// console.log(email)

	return User.findOneAndDelete({email: email}).then( result => {
		if(result){
			return true
		}
	})
}

module.exports.enroll = async (data) => {
	const {userId, courseId} = data


	//look for matching document of a user
	const userEnroll = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			// console.log(result)
			result.enrollments.push({courseId: courseId})

			return result.save().then( result => {
				return true
			})
		}

	})

	//look for matching document of a user
	const courseEnroll = await Course.findById(courseId).then( (result, err) => {
		if(err){
			return err
		} else {

			result.enrollees.push({userId: userId})

			return result.save().then( result => {
				return true
			})
		}
	})

	//to return only one value for the function enroll

	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}




    
 