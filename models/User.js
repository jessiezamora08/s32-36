const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
        firstName: {
            type: String, 
            required: [true, "first name is required"]
        }, 
        lastName: {
            type: String, 
            required: [true, "last name is required"]
        }, 
        email: {
            type: String, 
            required: [true, "e-mail is required"]
        },
        password: {
            type: String, 
            required: [true, "password is required"]
        },
        mobileNo: {
            type: String, 
            required: [true, "mobile number is required"]
        },
        age: {
            type: Number, 
            required: [true, "age is required"]

        }, 
        isAdmin: {
            type: Boolean, 
            default: false
        }, 
        enrollments: [
            {
                courseId:{
                    type: String, 
                    required : [true, "Course ID is required"]
                }, 
                enrolledOn: {
                    type: Date,
                    default: new Date()
                }, 
                status: {
                    type: String,
                    default: "Enrolled"
                }

            }
           
        ]

    }
);

//Create a model
	//.model(<model's name>, <schema>)


module.exports=mongoose.model("User", userSchema)