const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

const userController = require('../controllers/userContollers');

const auth = require('../auth')
//syntax: router.HTTPmethod('url', <request listener>)
//router.get('/',(req, res) => {
// 	console.log("Hello from userRoutes")
// })


//http://localhost:3001/users

// to verify if an email already exists
router.post('/email-exists', (req,res) => {
    userController.checkEmail(req.body.email)
    .then(result => res.send(result))
   
});

//register route 

router.post ('/register' , (req,res) => {
    //expecting data/ object from the request body
    userController.register(req.body).then(result => res.send(result))
})

// get all users


router.get('/', (req,res) => {
    userController.getAllUsers(req.body)
    .then(result => res.send(result))
})

router.post('/login', (req,res) => {
    userController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)

    userController.getUserProfile(userData).then(result => res.send(result))
})

router.put('/:userId/edit', (req, res) => {
	// console.log(req.params)
	// console.log(req.body)
	const userId = req.params.userId

	userController.editProfile(userId, req.body).then(result => res.send(result))
})

// http://localhost:3001/api/users/edit
router.put('/edit', auth.verify, (req, res) => {
	let userId = auth.decode(req.headers.authorization).id
	
	userController.editUser(userId, req.body).then( result => res.send(result))
})

//mini activity
	// create a route to update user details with the following:
		// endpoint = "/edit-profile"
		// function name: editDetails
		// method: put
		// use email as filter to findOneAndUpdate() method

router.put("/edit-profile", (req, res) => {
	// console.log(req.body)

	userController.editDetails(req.body).then(result => res.send(result))
})


//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/:userId/delete"
		// function name: delete()
		// method: delete
		// use id as filter to findByIdAndDelete method
		// return true if document was successfully deleted
router.delete("/:userId/delete", (req, res) => {
	// console.log(req.params.userId)

	userController.delete(req.params.userId).then(result => res.send(result))
})


//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/delete"
		// function name: deleteUser()
		// method: delete
		// use email as filter to findOneAndDelete method
		// return true if document was successfully deleted
router.delete("/delete", (req, res) => {
	// console.log(req.headers.authorization)
	// console.log( auth.decode(req.headers.authorization).email)
	const email = auth.decode(req.headers.authorization).email

	userController.deleteUser(email).then(result => res.send(result))
})

//enrollments 

router.post("/enroll",auth.verify, (req,res) => {
    let data = {
        //user id of the logged in user
        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId
    }
    userController.enroll(data).then(result => res.send(result))
})

module.exports = router;


