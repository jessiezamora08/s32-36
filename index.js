// initialize npm 

// npm init 
const express = require('express');


//express function is our server stored in a constant variable app
const app = express();

//require mongoose module to be used in our entry point file 

const mongoose = require('mongoose')
const cors = require('cors');

const port = 3001;
const User = require('./models/User')

app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());


const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.c1f1f.mongodb.net/course-booking?retryWrites=true&w=majority',
    {
        useNewUrlParser: true, 
        useUnifiedTopology:true,
    }
)

//notification 
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log ('connected to database'));


app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)

//server listening to port 3001
app.listen(port, () => console.log(`server running at ${port} !!! `))